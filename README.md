# theReadOnly.com

### Why?
By pareto principle, about 80% of internet websites suck. theReadOnly.com aims to suck less by listing websites that suck less on the internet. 

### How?
Inspired by the design of JustDeleteMe.xyz and Switching.software, theReadOnly.com is designed to be easily evolvable.

### Contributions
Contributions are welcome. 
